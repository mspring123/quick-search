# quick search

Search program and analytics for elasticsearch




> Author *`michaelspring123@web.de`*
> > Creation date *`2020-12-26`*
> > > version *`alpha`*


```mermaid
graph LR
    id1(start elastic instance)-->id2(poplute testdata)-->id3(search application)
    style id1 fill:#f9f,stroke:#333,stroke-width:4px
    style id2 fill:#bbf,stroke:#f66,stroke-width:2px,color:#fff,stroke-dasharray: 5 5
    style id3 fill:#f9,stroke:#333,stroke-width:2px
```

# start elastic instance

## locate the elastic folder

> asdf@asdfs-MBP Desktop % *`cd elasticsearch-7.10.1`*

<pre>
asdf@asdfs-MBP elasticsearch-7.10.1 % ls
LICENSE.txt    bin        jdk.app        modules
NOTICE.txt    config        lib        plugins
README.asciidoc    data        logs
asdf@asdfs-MBP elasticsearch-7.10.1 %
</pre>


> asdf@asdfs-MBP elasticsearch-7.10.1 % *`cd bin`*


<pre>
asdf@asdfs-MBP bin % ls
elasticsearch                elasticsearch-saml-metadata
elasticsearch-certgen            elasticsearch-setup-passwords
elasticsearch-certutil            elasticsearch-shard
elasticsearch-cli            elasticsearch-sql-cli
elasticsearch-croneval            elasticsearch-sql-cli-7.10.1.jar
elasticsearch-env            elasticsearch-syskeygen
elasticsearch-env-from-file        elasticsearch-users
elasticsearch-keystore            x-pack-env
elasticsearch-migrate            x-pack-security-env
elasticsearch-node            x-pack-watcher-env
elasticsearch-plugin
asdf@asdfs-MBP bin %
</pre>

## start the server

> asdf@asdfs-MBP bin % *`./elasticsearch`*

after the server has started, it can be reached at the following address

> *`http://localhost:9200/`*


```json
{
    "name": "asdfs-MBP.fritz.box",
    "cluster_name": "elasticsearch",
    "cluster_uuid": "2_66wUAhTjmUVoxT1OPm2g",
    "version": {
        "number": "7.10.1",
        "build_flavor": "default",
        "build_type": "tar",
        "build_hash": "1c34507e66d7db1211f66f3513706fdf548736aa",
        "build_date": "2020-12-05T01:00:33.671820Z",
        "build_snapshot": false,
        "lucene_version": "8.7.0",
        "minimum_wire_compatibility_version": "6.8.0",
        "minimum_index_compatibility_version": "6.0.0-beta1"
    },
    "tagline": "You Know, for Search"
}
```
